/**
 * @file
 * Initialization of data for Kantar Gallup spring script.
 */

var springq = springq || [];
springq.push({
  "s": drupalSettings.tns_gallup.site_id,
  "cp": drupalSettings.tns_gallup.content_path,
  "url": document.location.href
});
