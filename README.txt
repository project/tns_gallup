The official Danish internet traffic statistics from the Association
of Danish Media is handled by Kantar Gallup (previously TNS Gallup).

This module provides an easy method for adding the tracking script to
your Drupal site.

You must place the `spring.js` file you have received from Kantar
Gallup in a folder named `tns_gallup_spring` in the libraries folder.

When you have installed and enabled the module head over to
`admin/config/system/tns-gallup` and configure the Site ID provided to
you by Kantar Gallup.

The tracking script allows you to specify a ContentPath for filtering
and grouping of the statistics (i.e. specifying the site hierarchy).

Since how to set the ContentPath is actually business logic I decided
the module should just set it to the same as your Site ID and then
provide a hook for altering it. This way you can implement your own
business logic by implementing
`hook_tns_gallup_content_path_alter($content_path)`.

The module maintainer is not affiliated with Kantar Gallup or the
Association of Danish Media.
